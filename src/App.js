import "bootstrap/dist/css/bootstrap.css";
import React, { Component } from "react";
import NavBar from './components/NavBar'
import './app.css'
import { Container } from "react-bootstrap";
import Categories from "./pages/Categories";

export default class App extends Component {

  render() {
    return (
      <div>
        <NavBar/>
          <Container>
          <Categories/>
        </Container>
      </div>
    );
  }
}
