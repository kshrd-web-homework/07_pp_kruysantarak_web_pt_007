import React, {useState, useEffect} from 'react'
import { useParams, useHistory } from 'react-router';
import { Form, Button } from 'react-bootstrap';
import {addCategory} from '../Service/Services';
import { fetchAllCategories, deleteCategoryById, fetchCategoryById, updateCategory } from '../Service/Services';

export default function Categories() {

  const [categories, setCategories] = useState([])
  const [name, setName] = useState("")
  const [onUpdate, setOnUpdate] = useState(false);
  const [updateId, setUpdateId] = useState("");

  const history = useHistory();

  useEffect( () => {
    const fetchData = async () => {
      const result = await fetchAllCategories();
    setCategories(result);
    }
    fetchData();
  }, [name]);

  //deleteCategoryById
  const onDeleteCategory = async (id) => {
    const result = await deleteCategoryById(id);
      const temp = categories.filter(item=>{
        return item._id != id
      })
      setCategories(temp)
  }

  //addCategory
  const onAddCategory = async (e) => {
    e.preventDefault();
    const category = {name}
    const result = await addCategory(category);
    setName("")
  }

  //onFetchCategoryById
  const onFetchCategoryById = async (id) => {
    const result = await fetchCategoryById(id);
    setName(result.name);
    setUpdateId(id);
    setOnUpdate(true);
  }

  //onUpdateCategory
  const onUpdateCategory = async (e) => {
    e.preventDefault();
    const category = {name : name}
    console.log("update category : ", category)
    const result = await updateCategory(updateId, category);
    setOnUpdate(false)
    setName("")
  }

  return (
    <div>
      <div className="mb-2" style={{width:'50%'}}>
              <Form>
          <Form.Group controlId="formBasicEmail" >
            <Form.Control
              type="text"
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder="Category"
            />
          </Form.Group>
          <div className="d-flex">
          <Button
            onClick={onAddCategory}
            className="my-3 me-2"
            variant="primary"
            type="submit"
          >
            Add
          </Button>

          <Button style={onUpdate ? {display : "inline-block"}: {display : "none"}}
            onClick={onUpdateCategory}
            className="my-3"
            variant="primary"
            type="submit"
          >
            Update
          </Button>
          </div>

        </Form>
      </div>
      <div>
      <table className="table" id="table-data">
        <thead>
          <tr className="bg-success text-light">
            <th scope="col" style={{width : "10%"}}>#</th>
            <th scope="col" style={{width : "70%"}}>Category</th>
            <th scope="col" style={{width : "20%"}}>Action</th>
          </tr>
        </thead>
        <tbody>
          {categories.map(item => (
            <tr>
              <td>{item._id.slice(0, 10)}</td>
              <td>{item.name}</td>
              <td>
                <button onClick={() => onFetchCategoryById(item._id)}
                  type="button" className="btn btn-warning mb-3" >
                  Edit
                </button> {" "}
                <button onClick={() => onDeleteCategory(item._id)}
                    type="button" className="btn btn-danger mb-3" >
                  Delete
                </button>
              </td>
          </tr>
          ))}
        </tbody>
      </table>
    </div>
    </div>
  )
}
