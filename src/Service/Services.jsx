import axios from "axios";

//Base URL
export const API = axios.create({
  baseURL: "http://110.74.194.124:3034/api",
});

//fetch all categories
export const fetchAllCategories = async () => {
  try {
    const result = await API.get("/category");
    console.log("fetchAllCategories :", result);
    return result.data.data;
  } catch (error) {
    console.log("fetchAllCategories Error : ", error);
  }
};

//fetch category by id 
export const fetchCategoryById = async (id) => {
  try {
    const result  = await API.get(`/category/${id}`)
    return result.data.data
  } catch (error) {
    console.log("fetchCategroyById Error : ", error)
  }
}

//delete category by id
export const deleteCategoryById = async(id) => {
  try {
      const result = await API.delete(`/category/${id}`);
      return result.data.data;
    } catch (error) {
      console.log("deleteCategoryById Error : ", error);
    }
}

//Add category
export const addCategory = async(category) => {
  try {
    const result = await API.post("/category", category);
    return result.data.data
  } catch (error) {
    console.log("addCategory : ", error)
  }
}

//Update Category
export const updateCategory = async(id, category) => {
  try {
    const result = await API.put(`/category/${id}`, category)
    return result.data.data
  } catch (error) {
    console.log("updateCategory : ", error)
  }
}